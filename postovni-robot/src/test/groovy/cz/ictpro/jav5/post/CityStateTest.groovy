package cz.ictpro.jav5.post

import spock.lang.Specification

class CityStateTest extends Specification {

    def "Move"() {
        given:
        var state = new CityState(
                "Alice's House",
                [new Parcel("Alice's House", "Bob's House"),
                 new Parcel("Alice's House", "Cabin"),
                ]
        );
        when:
        var result = state.move("Farm")
        then:
        result.getRobotPlace() == "Bob's House"
        result.getParcels() == [new Parcel("Alice's House", "Cabin")]
    }
}
