package cz.ictpro.jav5.post

import spock.lang.Specification

class RandomRobotTest extends Specification {

    def "Otestovani generovani nahodného balíčku:"() {
        when:
        var p = RandomRobot.randomParcel(["A", "B", "C"])
        then:
        p == null
    }
}
