package cz.ictpro.jav5.post;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RandomRobot {

    private static Parcel randomParcel(List<String> places) {
        Collections.shuffle(places);
        return new Parcel(places.get(0), places.get(1));
    }

    private static final City CITY = City.getInstance();

    public static void main(String[] args) {
                var state = new CityState(
                "Post Office",
                Stream.generate(() -> randomParcel(new ArrayList<>(CITY.places())))
                        .limit(10)
                        .collect(Collectors.toList())
        );
        System.out.println(state);
        int count = 0;

        while (!state.getParcels().isEmpty()) {
            var neighbors = new ArrayList<>(CITY.neighbours(state.getRobotPlace()));
            Collections.shuffle(neighbors);

            var places = new ArrayList<>(CITY.places());
            state = state.move(neighbors.get(0), CITY);
            System.out.printf("%d. robot: %s, \tparcels: %d\n",
                    ++count,
                    state.getRobotPlace(),
                    state.getParcels().size());
        }
    }
}
