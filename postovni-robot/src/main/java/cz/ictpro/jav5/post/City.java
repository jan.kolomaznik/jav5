package cz.ictpro.jav5.post;

import java.io.*;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import static java.util.Collections.*;


/**
 * Object reprezentující graf města
 */
public class City {

    private static City instance;

    public static City getInstance() {
        if (instance == null) {
            File source = new File("village2x.txt");
            try (BufferedReader br = new BufferedReader(new FileReader(source))) {

                instance = new City();
                var cons = instance.builder();
                br.lines().forEach(line -> {
                    var split = line.split("-");
                    cons.accept(split[0], split[1]);
                });
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public BiConsumer<String, String> builder() {
        return (a, b) -> {
            if (!graph.containsKey(a)) graph.put(a, new HashSet<>());
            if (!graph.containsKey(b)) graph.put(b, new HashSet<>());
            graph.get(a).add(b);
            graph.get(b).add(a);
        };
    }

    /**
     * Mapa kde líč je název místa a hodnota je množina sousedících míst
     */
    private Map<String, Set<String>> graph = new LinkedHashMap<>();

    /**
     * Vrací seznam jmen budov ve městě
     * @return unmoficable list
     */
    public Collection<String> places() {
        return graph.keySet();
    }

    /**
     * Otestuje, jestli existuje přímá cesta mezi dvěmi místy ve městě.
     * @return <code>true</code> pokud existuje
     */
    public boolean isNeighbours(String a, String b) {
        return graph.getOrDefault(a, emptySet()).contains(b);
    }

    public Collection<String> neighbours(String place) {
        return unmodifiableCollection(graph.get(place));
    }
}
