package cz.ictpro.jav5.post;

import lombok.Value;
import lombok.With;

import java.util.Objects;

@Value @With
public class Parcel {

    private final String currentPlace;
    private final String destination;

    public boolean isDelivery() {
        return Objects.equals(currentPlace, destination);
    }
}
