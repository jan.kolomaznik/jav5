package cz.ictpro.jav5.post;

import lombok.Value;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;
import static java.util.stream.Collectors.toList;

@Value
public class CityState {

    private static final City CITY = City.getInstance();

    private final String robotPlace;
    private final Collection<Parcel> parcels;

    public CityState move(String where, City city) {
        // Pokud se chci pohnout někam kam nemužu, tak se nic neděje.
        if (!CITY.isNeighbours(robotPlace, where)) return this;
        return new CityState(
                where,
                parcels.stream()
                    .map(p -> Objects.equals(p.getCurrentPlace(), robotPlace) // Pokud se ma balik přesunou s robotem
                            ? p.withCurrentPlace(where) // Balik se presune na novou pozici
                            : p) // jinak se s nim nic neděje.
                    .filter(not(Parcel::isDelivery))
                    .collect(toList()));
    }

}
