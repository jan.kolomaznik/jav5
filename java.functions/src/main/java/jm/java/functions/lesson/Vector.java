package jm.java.functions.lesson;

public interface Vector {

    double getX();
    double getY();

    default double size(){
        return Math.sqrt(getX()*getX() + getY()*getY());
    }

}
