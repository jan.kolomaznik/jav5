package jm.java.functions.lesson;

public class ModVector implements Vector {

    private double x, y;
    int mod = 10;

    @Override
    public double getX() {
        return x % mod;
    }

    @Override
    public double getY() {
        return y % mod;
    }
}
