package jm.java.functions.lesson;

public class BaseClass {

    static int staticInt;

    private String value;


    public BaseClass(String value) {
        this.value = value;
    }

    static class StaticInner {

    }

    class Inner {
        int value;

        @Override
        public String toString() {
            return BaseClass.this.value;
        }
    }

    public static void main(String[] args) {
        BaseClass.staticInt = 1;

        StaticInner si = new StaticInner();
        System.out.println(si.toString());

        BaseClass baseClass = new BaseClass("Test");

        Inner inner = baseClass.new Inner();
        System.out.println(inner.toString());
    }
}
