import jm.java.functions.lesson.MyVector;

import java.util.Comparator;
import java.util.function.ToIntBiFunction;
import java.util.function.ToIntFunction;

@FunctionalInterface
public interface MyComparator extends Comparator<String> {

    @Override
    int compare(String o1, String o2);

    default int invCompare(String o1, String o2) {
        return -compare(o1, o2);
    }

    public static void main(String[] args) {

        MyComparator sc = new MyComparator() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        };

        System.out.println(sc.compare("A", "B"));
        System.out.println(sc.invCompare("A", "B"));

        MyVector myVector = new MyVector();
        System.out.println(myVector.size());
    }

}


