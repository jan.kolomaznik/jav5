package jm.lombok;

import lombok.*;

import java.time.LocalDate;

// tag::Data[]
@Data
public class PersonLombok_4 {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        PersonLombok_4 person = new PersonLombok_4();
        person.setFirstName("Pepa");
        person.setLastName("Zeepa");
        System.out.println(person);
        System.out.println("Equals: " + person.equals(
                new PersonLombok_4()
        ));
    }
}
// end::Data[]

