import java.util.*;
import java.util.stream.Collectors;

public class For {

    enum Gender {
        MALE, FEMALE;
    }

    static class People {

        public static boolean hightIQ(People people) {
            return people.iq > 110;
        }

        int iq;
        int age;
        String name;
        Gender gender;

        public People(int iq, int age, String name, Gender gender) {
            this.iq = iq;
            this.age = age;
            this.name = name;
            this.gender = gender;
        }

        boolean goodIQ(int iq) {
            return iq > 110;
        }

        @Override
        public String toString() {
            return "People{" +
                    "iq=" + iq +
                    ", age=" + age +
                    ", name='" + name + '\'' +
                    ", gender=" + gender +
                    '}';
        }
    }

    static class Employ {
        String name;
        double salary;
        int level;

        public Employ(String name, double salary) {
            this.name = name;
            this.salary = salary;
        }

        @Override
        public String toString() {
            return "Employ{" +
                    "name='" + name + '\'' +
                    ", salary=" + salary +
                    ", level=" + level +
                    '}';
        }
    }

    public static void main(String[] args) {
        var people = Arrays.asList(
                new People(120, 23, "Pepa", Gender.MALE),
                new People(135, 18, "Anna", Gender.FEMALE),
                new People(101, 23, "Tom", Gender.MALE),
                new People(95,  55, "Jura", Gender.MALE),
                new People(150, 45, "Jana", Gender.FEMALE)
        );

        // iq > 110
        // salary = iq * age * 100 (if female * 0.9)

        var step_1 = new ArrayList<People>();
        for (var p : people) {
            if (p.iq > 110) {
                step_1.add(p);
            }
        }

        var step_2 = new ArrayList<Employ>();
        for (var p : step_1) {
            var e = new Employ(
                    p.name,
                    p.iq * p.age * 100 * ((p.gender == Gender.MALE) ? 1 : 0.9));
            step_2.add(e);
        }

        var step_3 = 0.0;
        for (var c : step_2) {
            step_3 += c.salary;
        }

        System.out.println(step_2);
        System.out.println(step_3);

        // Stream varinata
        var linka = people.stream()
                .filter(p -> p.iq > 110)
                .map(p -> new Employ(
                        p.name,
                        p.iq * p.age * 100 * ((p.gender == Gender.MALE) ? 1 : 0.9)))
                .peek(System.out::println)
                .mapToDouble(c -> c.salary);
        System.out.println(linka.sum());

        people.stream().filter(People::hightIQ);
        //people.stream().filter(People::goodIQ());
        people.stream().filter(p -> p.goodIQ(110));


        var map = people.stream()
                .reduce(new HashMap<Gender, List<People>>(),
                        (m, p) -> {
                            if (!m.containsKey(p.gender)) {
                                m.put(p.gender, new ArrayList<People>());
                            }
                            m.get(p.gender).add(p);
                            return m;
                        },
                        (a, b) -> a);
        System.out.println(map);

        System.out.println(people.stream()
                .collect(Collectors
                        .groupingBy(p -> p.gender)));
    }


}
