package jm.java.streams;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Memory {

    // [1,2,3,4,5,6,7],3 = [[1,2,3],[4,5,6]]

    public static List<List<Long>> byFor(Iterator<Long> iter, int n) {
        var result = new ArrayList<List<Long>>();
        main: while (iter.hasNext()) {
            var step = new ArrayList<Long>();
            for (int i = 0; i < n; i++) {
                if (iter.hasNext()) {
                    step.add(iter.next());
                } else {
                    break main;
                }
            }
            result.add(step);
        }
        return result;
    }

    public static List<List<Long>> byStream(Stream<Long> iter, int n) {
        // TODO
        return null;
    }

    public static void main(String[] args) {
        var numbers = LongStream
                .range(0, 1_000_000_000)
                .mapToObj(l -> new Long(l))
                .iterator();
        System.out.println(byFor(numbers, 3));
    }
}
