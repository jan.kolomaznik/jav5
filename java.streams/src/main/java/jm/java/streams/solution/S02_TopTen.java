package jm.java.streams.solution;

import jm.java.streams.exercise.TopTen;
import jm.java.streams.exercise.TimePoint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.NavigableSet;
import java.util.Random;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A solution/demonstration of {@link TopTen}.
 *
 * @author Petr Dolezal
 */
public final class S02_TopTen {

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     */
    public static void main(String... args) {
        // The solution here is threefold, providing comparison of various ways:
        // - an intermediate collection (similar to the common approach)
        // - pure stream-based solution
        // - a solution using explicit sorting instead of pure stream
        final int size = 10_000_000; // Change and see the differences
        final int count = 10;

        // TODO: Yet try what happens with parallel streams

        { // The case of collection-based result
            final TimePoint measure = new TimePoint();
            streamingTop(count, collectionOf(size).stream());
            System.out.println(measure.toString(TimeUnit.MILLISECONDS));
            System.out.println();
        }
        { // Streaming without any collection
            final TimePoint measure = new TimePoint();
            streamingTop(count, streamOf(size));
            System.out.println(measure.toString(TimeUnit.MILLISECONDS));
            System.out.println();
        }
        { // Using a tree-based intermediate sorter
            final TimePoint measure = new TimePoint();
            treeTop(count, streamOf(size));
            System.out.println(measure.toString(TimeUnit.MILLISECONDS));
            System.out.println();
        }
    }

    /**
     * Prints the greatest elements from the stream.
     *
     * <p>
     * Fewer elements than specified can be printed if the stream does not
     * provide enough elements.
     *
     * @param count
     *            the count of elements to print. It must not be negative.
     * @param input
     *            the input stream. It must not be {@code null} and it must not
     *            contain {@code null} elements.
     */
    private static <T extends Comparable<? super T>> void streamingTop(int count, Stream<? extends T> input) {
        input.sorted(Comparator.reverseOrder()).limit(count).forEachOrdered(System.out::println);
    }

    /**
     * Prints the greatest elements from the stream.
     *
     * <p>
     * Fewer elements than specified can be printed if the stream does not
     * provide enough elements.
     *
     * @param count
     *            the count of elements to print. It must not be negative.
     * @param input
     *            the input stream. It must not be {@code null} and it must not
     *            contain {@code null} elements.
     */
    private static <T extends Comparable<? super T>> void treeTop(int count, Stream<? extends T> input) {
        final Comparator<T> comparator = Comparator.reverseOrder();
        final NavigableSet<T> top = new TreeSet<>(comparator);
        input.forEachOrdered(o -> {
            if (top.add(o) && (top.size() > count)) {
                top.pollLast();
            }
        });

        top.forEach(System.out::println);
    }

    /**
     * Makes a collection of the given size.
     *
     * @param count
     *            the size of the collection. It must not be negative.
     *
     * @return the collection
     */
    private static Collection<Integer> collectionOf(int count) {
        return streamOf(count).collect(Collectors.toCollection(() -> new ArrayList<>(count)));
    }

    /**
     * Makes a stream of the given length.
     *
     * @param count
     *            the length of the stream. It must not be negative.
     *
     * @return the stream
     */
    private static Stream<Integer> streamOf(int count) {
        return new Random().ints(count, 0, count * 2).mapToObj(Integer::valueOf);
    }
}
