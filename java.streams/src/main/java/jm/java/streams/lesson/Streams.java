package jm.java.streams.lesson;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Examples of using streams.
 */
public final class Streams {

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     *
     * @throws Exception
     *             if something goes wrong
     */
    public static void main(String... args) throws Exception {

        // print would repeat many times, let's make it shorter
        final Consumer<Object> print = System.out::println;

        // Stream.of() and Stream.forEach()
        // This is obvious, isn't it?
        Stream.of(1, 2, 3).forEach(print);
        // With an intermediate collection we could write
        Arrays.asList(1, 2, 3).stream().forEach(print);

        // Stream.iterate() and Stream.limit()
        // Generates an infinite sequence using a generative function. Because
        // the sequence is infinite, we have to apply here limit() to avoid
        // endless execution. Each element is printed using System.out.
        var st = Stream.iterate("a", s -> s + "a").limit(10);
        st.forEach(print);

        // Stream.generate()
        // Similar to previous case, just the stream provides a ticks of the
        // system clock - no previous value is provided
        Stream.generate(System::nanoTime).limit(10).forEach(print);

        // Stream.skip()
        // Also obvious complement to limit()
        Stream.of("a", "b", "c").skip(2).forEach(print);

        // Stream.map()
        // Transformation of the elements on the fly.
        Stream.of(1, 2, 3).map(i -> -i).forEach(print);

        // Stream.filter()
        // A condition which passes further only matching elements.
        Stream.of(1, 2, 3).filter(i -> i % 2 == 0).forEach(print);

        // Stream.flatMap()
        // A comprehensible example using an actual map which is flattened
        final Map<String, Collection<String>> map = new HashMap<>();
        map.put("a", Arrays.asList("Hello", "World"));
        map.put("b", Arrays.asList("Hello", "Dolly"));
        map.put("z", Arrays.asList("good", "bye"));
        // Flattening of all contained values in a simple sequence (unrolling the values)
        map.values().stream().flatMap(value -> value.stream()).forEach(print);
        // The flattening mapping function provides the values and flatMap
        Stream.of("a", "z").flatMap(key -> map.get(key).stream()).forEach(print);
        // The output can be calculated, the elements don't have to exist before.
        Stream.of(1, 2, 3).flatMap(n -> Stream.generate(() -> n).limit(n)).forEach(print);

        // We'll use this more times
        final Collection<String> names = Arrays.asList("Hector", "Sylvester", "Granny");

        // Stream.peek()
        // An operation within the pipeline which is just invoked on the elements
        final Consumer<Object> tweety = what -> {
            if ("Sylvester".equals(what)) {
                System.out.println("I saw a putty tat!");
            }
        };

        names.stream().peek(tweety).forEach(print);

        // Stream.sorted()
        // Quite obvious, isn't it?
        names.stream().sorted().forEach(print);

        // Stream.distinct()
        // Remove duplicated values
        Stream.of(4, 1, 2, 3, 1).distinct().forEach(print);

        // Stream.findFirst() and Stream.findAny(), Optional
        final Optional<String> name = names.stream().findFirst();
        // Two ways of dealing with optional content: conditional or pipelined
        name.ifPresent(s -> System.out.println("Once again " + s));
        if (name.isPresent()) {
            System.out.println(name.get());
        }

        // There is complete if-else-throw support
        System.out.println(Collections.emptySet().stream().findAny().orElse("Nothing is here!"));

        // Stream.min() and Stream.max()
        // This comparator is actually equal to Comparator.naturalOrder()
        final Comparator<String> comparator = (a, b) -> a.compareTo(b);
        System.out.println(names.stream().max(comparator).get());
        System.out.println(names.stream().min(comparator).get());

        // Stream.reduce()
        // Reduction is an aggregating operation on the elements
        System.out.println(names.stream().map(String::length).reduce(0, (a, b) -> a + b));
        // This is a more efficient variant which uses the special version for int
        System.out.println(names.stream().mapToInt(String::length).reduce(0, (a, b) -> a + b));
        // When talking about special versions of streams...
        final String[] array1 = names.stream().toArray(length -> new String[length]);
        IntStream.range(0, array1.length).forEach(i -> print.accept(array1[i]));
        // Well, this is remarkable: we already know about T::new notation, but for arrays...
        final String[] array2 = names.stream().toArray(String[]::new);
        System.out.println(Arrays.toString(array2));

        // A version of reduce which is useful when there is no suitable neutral
        // element (or we want to use it for a reason to distinguish the case of
        // the stream being empty)
        Stream.of(1, 2, 3).reduce((a, b) -> a + b).ifPresent(print);

        // Stream.collect()
        // Similar to reduce, but it uses a mutable target that accumulates the result;
        // there is a lot of predefined collectors for common situations like this one
        System.out.println(names.stream().collect(Collectors.joining(", ", "[", "]")));

        // And there are other places where it can be met
        try (Stream<Path> files = Files.list(Paths.get("."))) {
            files.forEach(print);
        }
    }
}
