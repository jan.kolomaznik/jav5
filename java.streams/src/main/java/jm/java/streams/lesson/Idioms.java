package jm.java.streams.lesson;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * A few idioms, patterns and techniques used commonly in functional programming
 * and Java 8 extensions.
 */
public final class Idioms {

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     */
    public static void main(String... args) {
        final Consumer<Object> print = System.out::println;

        // Combining simple interfaces with default methods and lambdas
        //
        // See the definition of the interface: it provides all necessary to
        // form a predicate and lambda can nicely fit in the only small missing
        // piece: the test() method
        final Predicate<Person> isAdult = p -> (p.age >= 18);
        final Predicate<Person> isFemale = p -> (p.sex == Sex.FEMALE);
        final Predicate<Person> isHealthy = p -> {
            final double h = p.height / 100.0;
            final double bmi = p.weight / (h * h);
            return ((bmi > 18.5) && (bmi < 25));
        };

        // Immutable objects
        //
        // Again, lambda nicely fits this approach as it does not have state
        // directly (it can be stateful only via a binding to an external
        // stateful object).

        // Composition and chaning
        // Even immutable (or possibly: because of being immutable) the common
        // technique is composition of small objects and chaining is used
        // heavily.
        final Predicate<Person> condition = isAdult.and(isFemale).and(isHealthy);

        // SELECT from WHERE condition...
        // The logic is similar to database queries: from a source filter the
        // data to operate on, transform them or aggregate, sort/apply distinct,
        // limit the output etc. and finally use somehow.
        PERSONS.stream().filter(condition).sorted((a, b) -> Integer.compare(a.age, b.age)).limit(2).forEach(print);
        System.out.println(PERSONS.stream().filter(condition).count()); // Showing that limit() works

        // Just for completeness an example of a more readable comparator which
        // is made from a value-to-compare extraction function
        PERSONS.stream().filter(condition).sorted(Comparator.comparing(p -> p.age)).limit(2).forEach(print);

        // Reduction
        // It's the common aggregating operation and it is supported by Stream.reduce()
        // and Stream.collect(). The latter one is more complicated and it actually
        // combines map() and reduce() together in a way.

        // This is a simple reduction in its pure form
        System.out.println(PERSONS.stream().mapToInt(p -> p.weight).reduce(0, (a, b) -> a + b));

        // An example of aggregated data using a (predefined) collector
        System.out.println(PERSONS.stream().collect(Collectors.averagingDouble((Person p) -> p.age)));
        System.out.println(PERSONS.stream().mapToDouble(p -> p.age).average().getAsDouble());
        // Partitioning
        // Splitting a group into two depending on a predicate
        System.out.println(PERSONS.stream().collect(Collectors.partitioningBy(isHealthy)));

        // Grouping
        // Splitting a group according to a value
        System.out.println(PERSONS.stream().collect(Collectors.groupingBy((Person p) -> p.sex)));
    }

    // Some stuff for richer testing data, not documented properly though

    enum Sex {
        MALE, FEMALE, UNSPECIFIED;
    }

    static final class Person {

        public final String nick;
        public final int weight;
        public final int height;
        public final int age;
        public final Sex sex;

        public Person(String n, Sex s, int a, int h, int w) {
            weight = w;
            height = h;
            age = a;
            sex = s;
            nick = n;
        }

        @Override
        public String toString() {
            final StringJoiner result = new StringJoiner(", ", "{", "}");
            result.add(nick).add(sex.toString()).add(Integer.toString(age));
            result.add(Integer.toString(height)).add(Integer.toString(weight));
            return result.toString();
        }
    }

    // @formatter:off
    static final Collection<Person> PERSONS = Collections.unmodifiableCollection(Arrays.asList(
            new Person("Sue's mother", Sex.FEMALE, 71, 165, 60),
            new Person("Sue", Sex.FEMALE, 48, 160, 50),
            new Person("Sue's daughter", Sex.FEMALE, 16, 170, 50),
            new Person("Mick", Sex.MALE, 25, 175, 75),
            new Person("Fat Betty", Sex.FEMALE, 30, 170, 100),
            new Person("Nice Ally", Sex.FEMALE, 28, 170, 55),
            new Person("Rene", Sex.UNSPECIFIED, 22, 175, 65)
        ));
    // @formatter:on
}
