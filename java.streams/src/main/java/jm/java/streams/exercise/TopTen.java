package jm.java.streams.exercise;

import java.util.Collection;
import java.util.Comparator;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Exercise: print N greatest numbers from the given collection.
 */
public final class TopTen {

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     */
    public static void main(String... args) {
        final int count = 1000;
        top(10, new Random().ints(count, 0, count * 2).mapToObj(Integer::valueOf).collect(Collectors.toList()));
    }

    /**
     * Prints the greatest elements from the collection.
     *
     * <p>
     * Fewer elements than specified can be printed if the collection does not
     * contain enough elements.
     *
     * @param count
     *            the count of elements to print. It must not be negative.
     * @param input
     *            the input collection. It must not be {@code null} and it must
     *            not contain {@code null} elements.
     */
    private static void top(int count, Collection<? extends Number> input) {
        input.stream()
                .sorted((a, b) -> b.intValue() - a.intValue())
                .limit(count)
                .forEach(System.out::println);
    }
}
