package jm.java.streams.exercise;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Exercise: resolve the tasks in the code below.
 */
public final class CollectionBasics {

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     */
    public static void main(String... args) {
        var random = new Random();

        final List<Integer> numbers = Stream
                .generate(random::nextInt).limit(100) // TODO: Make a list of random numbers
                .filter(i -> i % 2 == 0)       //TODO: Remove all even numbers
                .sorted()                      // TODO: Sort the list
                .peek(System.out::println)     // TODO: Print the remaining numbers, one on each line
                .collect(Collectors.toList()); // TODO: Do it with lambda and with new methods on the collection
        System.out.println(numbers);
        // TODO: Rewrite all above with a single stream and no intermediate collection
    }
}
