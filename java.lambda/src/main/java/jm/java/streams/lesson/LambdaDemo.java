package jm.java.streams.lesson;

public class LambdaDemo {

    /*
    interface Comparable {
        public int compareTo(Object o);
    }
    */


    public static void main(String[] args) {

        Object etalon = "Popa";

        Comparable comp = new Comparable() {

            @Override
            public int compareTo(Object o) {
                //etalon = "Honza";
                return 0;
            }
        };

        comp.compareTo("Pepa");

    }
}
