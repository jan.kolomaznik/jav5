package jm.java.streams.lesson;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SwingDemo extends JPanel {

    public SwingDemo() {
        JButton btn = new JButton("Click");
        btn.addActionListener(this::onClick);
        add(btn);
    }

    void onClick(ActionEvent e) {
        System.out.println("Click");
    }

    public static void main(String[] args) {
        JFrame jFrame = new JFrame();
        jFrame.setContentPane(new SwingDemo());
        jFrame.pack();
        jFrame.setVisible(true);
    }
}
