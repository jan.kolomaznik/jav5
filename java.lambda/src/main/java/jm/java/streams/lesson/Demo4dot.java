package jm.java.streams.lesson;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.function.LongFunction;
import java.util.function.Supplier;

public class Demo4dot {

    static class Sum {
        int value = 0;
    }

    public static void main(String[] args) {
        List<String> col = Arrays.asList("Ahoj", "from", "for", "cycle");
/*
        int sum = 0;
        for (String str : col) {
            sum += str.length();
        }
        System.out.println(sum);

        int fSum = 0;
        col.forEach(str -> fSum+= str.length());
        System.out.println(fSum);


        Sum sSum = new Sum();
        col.forEach(str -> sSum.value+= str.length());
        System.out.println(sSum.value);
*/
        Function<Long, Date> dateFromLong = Date::new;
        Supplier<Date> dateCurrent = Date::new;

        System.out.println(dateFromLong.apply(1L));
        System.out.println(dateCurrent.get());



    }
}
