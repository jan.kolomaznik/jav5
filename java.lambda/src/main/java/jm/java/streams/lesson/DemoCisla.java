package jm.java.streams.lesson;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class DemoCisla {

    public interface Condition<T> extends Predicate<T> {

        default Consumer<T> applyOn(Consumer<? super T> consumer) {
            Objects.requireNonNull(consumer);
            return value -> {
                if (test(value)) {
                    consumer.accept(value);
                }
            };
        }
    }

    public static void main(String[] args) {
        Condition<Integer> kladna = i -> i > 0;
        Consumer<Integer> vypis = System.out::println;

        Consumer<Integer> vypisKladna = kladna.applyOn(vypis);
        vypis.accept(1);
        vypisKladna.accept(1);
        vypisKladna.accept(-1);

    }

}
