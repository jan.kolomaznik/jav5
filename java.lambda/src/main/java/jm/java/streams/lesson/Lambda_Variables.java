package jm.java.streams.lesson;

import java.util.Arrays;
import java.util.Collection;

/**
 * Examples of using a more complex lambda instances.
 */
public final class Lambda_Variables {

    /**
     * A home-made version of {@link ToIntFunction} (assuming this was available
     * earlier).
     *
     * @param <T>
     *            the type of argument
     */
    private interface ToIntFunction<T> {

        /**
         * Applies this function to the given argument.
         *
         * @param value
         *            the function argument
         *
         * @return the function result
         */
        int apply(T value);
    }

    /**
     * Prints transformed values.
     *
     * @param values
     *            the values to transform and print; it must not be {@code null}
     * @param f
     *            the transformation to apply; it must not be {@code null}
     */
    private static <T> void printResults(Iterable<? extends T> values, ToIntFunction<T> f) {
        for (T value : values) {
            System.out.format("%s -> %d%n", value, f.apply(value));
        }
    }

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     */
    public static void main(String... args) {
        final Collection<String> values = Arrays.asList("one", "two", "three", "");

        // Lambda body can be quite complex and use whatever a normal function can
        printResults(values, s -> {
            int result = 0;
            for (int i = s.length(); i-- > 0;) {
                ++result;
            }

            return result;
        });

        // Lambda body can use variables of the enclosing scope
        final int someFinalValue = 42; // Here could be a complex calculation
        printResults(values, o -> someFinalValue);

        // The variable needn't be declared final, but it must be effectively final
        int someVariable = someFinalValue + 1;
        printResults(values, o -> someVariable);
        //someVariable = 1; // FIXME: Such a change is not allowed then!
    }
}
