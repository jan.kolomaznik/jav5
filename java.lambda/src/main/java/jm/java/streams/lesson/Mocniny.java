package jm.java.streams.lesson;

import java.util.function.Function;

public class Mocniny {

    interface Operator {
        Double op(Double a, Double b);
    }

    public static void main(String[] args) {
        Operator pow = (a,b) -> Math.pow(a,b);
        Function<Double, Runnable> pow2 = z -> () -> pow.op(z, 2.0);

        pow2.apply(4.0).run();
    }
}
