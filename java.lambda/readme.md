[remark]:<class>(center, middle)
# Lamda výrazy

[remark]:<slide>(new)
## Úvod
Představují způsob, jak definovat jako objekt nějakou část kódu, kterou bychom v jiné části programu rádi použili.

Lambda výraz můžeme uložit do proměnné funkčního typu a předávat metodám jako hodnotu parametru funkčního typu.

Překladač definuje lambda výraz jako instanci funkčního rozhraní, jehož metoda má odpovídající parametry a vrací hodnotu odpovídajícího typu (převod na příslušný typ zařídí překladač).

[remark]:<slide>(wait)
#### Funkční rozhraní na lambda
[Lambda_Syntax.java](src/main/java/jm/java/lambda/lesson/Lambda_Syntax.java)

[remark]:<slide>(new)
## Možnosti zápisu
Lambda výraz zapisujeme některým z následujících způsobů:
```java
(parametry) -> { příkazy } //Obecný tvar
parametr    -> { příkazy } //Jediný parametr
(parametry) -> výraz //Tělo tvoří pouze vyhodnocovaný výraz
parametr    -> výraz //Jediný parametr + pouze 1 výraz
```

Je-li vlevo jediný parametr, nemusí se dávat do kulatých závorek. 

Není-li žádný, nebo je-li jich více, závorky jsou potřeba.

Dokáže-li si překladač odvodit typ parametru, nemusí se uvádět.

Pokud se vpravo pouze vyhodnocuje nějaký výraz, nemusí se dávat do složených závorek.

[remark]:<slide>(new)
#### Příklad
```java
@FunctionalInterface
public interface MojeFunkce {
    public int počítej(int vstup);
}
```

Funkci implementující rozhraní MojeFunkce můžeme vytvořit několika způsoby:

```java
MojeFunkce naDruhou = (int i) -> {
    return i * i;
};
MojeFunkce naDruhou = (i) -> i * i;
MojeFunkce naDruhou = i -> i * i;
```

[remark]:<slide>(wait)
#### Práce s lambdou jako s proměnou
[Lambda_Variables.java](src/main/java/jm/java/lambda/lesson/Lambda_Variables.java)


[remark]:<slide>(new)
## Odkazy na metody a konstruktory
Pomocí dvou dvojteček (`::`) se můžeme odkazovat na metody nebo konstruktory a konvertovat je na funkce, resp. funkční rozhraní. 

#### Namísto: 
```java
Supplier<Long> dodavatel = () -> System.currentTimeMillis();
```

#### napíšeme:
```java
Supplier<Long> dodavatel = 	System::currentTimeMillis;
```

a v obou případech použijeme např. takto:

```java
long cas = dodavatel.get();
```

[remark]:<slide>(new)
## Další příklady použití dvou dvojteček:

#### Pro metody instance:
```java
Date datum = new Date();
Supplier<Long> dodavatel = datum::getTime;
long čas = dodavatel.get();
```

#### Pro konstruktory:
```java
Function<Long, Date> generátorData = Date::new;
Date datum = generatorData.apply(System.currentTimeMillis());
System.out.println(datum);
```

Metoda nebo konstruktor musí mít samozřejmě vhodný počet a typ parametrů, které odpovídají požadovanému funkčnímu rozhraní. 

Výše byl zavolán konstruktor `public Date(long date)`.

[remark]:<slide>(wait)
#### Práce s referencemi:
[Lambda_Reference.java](src/main/java/jm/java/lambda/lesson/Lambda_Reference.java)

[remark]:<slide>(new)
[remark]:<class>(center, middle)
# Cvičení

Projděte příklad:

[Lambda_Traps.java](src/main/java/jm/java/lambda/lesson/Lambda_Traps.java)

[Lambda_Together.java](src/main/java/jm/java/lambda/lesson/Lambda_Together.java)

