import java.util.Optional;
import java.util.regex.Pattern;

public class NullFight {

    private static final Pattern pattern = Pattern.compile("(^|\\s)([A-Z]\\w+)($|\\s)");
    /**
     * Najde slovo začínající velkým písmenem ve věte a to vrtádí jako jméno.
     */
    public static Optional<String> findName(String sentence) {
        var m = pattern.matcher(sentence);
        return m.find()
                ? Optional.of(m.group(2))
                : Optional.empty();
    }

    public static void main(String[] args) {
        var result = findName("mesto Adamov je krasné");
        System.out.println(result.get());
        System.out.println(result.map(String::toUpperCase));
        System.out.println(result
                .filter(n -> n.startsWith("A"))
                .map(String::toUpperCase)
                .orElseThrow(IllegalArgumentException::new)
        );
    }

}
