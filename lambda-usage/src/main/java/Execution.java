import java.util.concurrent.*;

public class Execution {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService es = Executors.newFixedThreadPool(3);

        for (int i = 0; i < 5; i++) {
            final int c = i;
            es.submit(() -> {
                try {
                    Thread.sleep(3000);
                    System.out.println("Done " + c);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }

        Future<String> fut = es.submit(() -> {
            try {
                Thread.sleep(3000);
                return "Result";
            } catch (InterruptedException e) {
                return "Catch";
            }
        });

        System.out.println(fut.isDone());
        System.out.println(fut.get());
    }
}
