import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NullFightTest {

    @Test
    void findName() {
        var sentens = "mesto praha ma 100 věží";

        var oRes = NullFight.findName(sentens);

        assertAll(
                () -> assertNotNull(oRes),
                () -> assertTrue(oRes.isPresent()),
                () -> assertEquals("Praha", oRes.get())
        );
    }
}